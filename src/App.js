import React, { useState } from "react";



const App = () => {

  const [count, setCount] = useState(0)
  const [user, setUser] = useState('')

  const increment = () => {
    setCount(count + 1)
  }

  const handleOnChange = (e) => {
    setUser(e.target.value)
    console.log("user: ", user)
  }

  return (
    <>
      <div>Count: {count}</div>
      <button onClick={increment}>Increment</button>
      <br />
      <input onChange={handleOnChange} value={user} />

    </>
  )
}


export default App